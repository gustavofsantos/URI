#!/usr/bin/python3
# -*- coding: utf-8 -*-
from datetime import date

while True:
	try:
		linha = input()
		mes, dia = linha.split(' ')
		mes = int(mes)
		dia = int(dia)

		if mes == 12 and dia == 25:
			print('E natal!')
		elif mes == 12 and dia == 24:
			print('E vespera de natal!')
		elif mes == 12 and dia > 25:
			print('Ja passou!')
		else:
			natal = date(2016, 12, 25)
			hoje = date(2016, mes, dia)
			dif = abs(natal - hoje).days 
			print('Faltam {} dias para o natal!'.format(dif))
	except:
		break
	